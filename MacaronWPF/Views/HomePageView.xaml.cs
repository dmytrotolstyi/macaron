﻿using MacaronWPF.Structures;
using MacaronWPF.Utils;
using System.Runtime.InteropServices;
using System.Windows.Controls;

namespace MacaronWPF.Views
{
    /// <summary>
    /// Interaction logic for HomePageView
    /// </summary>
    public partial class HomePageView : UserControl
    {
        public HomePageView()
        {
            InitializeComponent();
        }
    }
}
