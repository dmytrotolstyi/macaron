﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MacaronWPF.Services
{
    class ProcessService
    {
        delegate bool EnumThreadDelegate(IntPtr hWnd, IntPtr lParam);
        [DllImport("user32.dll")]
        static extern bool EnumThreadWindows(int dwThreadId, EnumThreadDelegate lpfn, IntPtr lParam);

        public static Process[] GetProcessByName(String name)
        {
            return Process.GetProcessesByName(name);
        }

        public static IEnumerable<Window> GetWindowsByProcess(Process process)
        {
            var windows = new List<Window>();

            foreach (IntPtr hWnd in EnumerateProcessWindowHandles(process))
            {
                windows.Add(new Window(hWnd));
            }

            return windows;
        }

        private static IEnumerable<IntPtr> EnumerateProcessWindowHandles(Process process)
        {
            var handles = new List<IntPtr>();

            foreach (ProcessThread thread in process.Threads)
            {
                EnumThreadWindows(thread.Id, (hWnd, lParam) => { handles.Add(hWnd); return true; }, IntPtr.Zero);
            }

            return handles;
        }
    }

}
