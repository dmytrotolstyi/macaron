﻿using MacaronWPF.Structures;
using MacaronWPF.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MacaronWPF.Services
{
    [Obsolete("Use Window Model directly")]
    class WindowService
    {
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetDC(IntPtr window);
        [DllImport("gdi32.dll", SetLastError = true)]
        public static extern uint GetPixel(IntPtr dc, int x, int y);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int ReleaseDC(IntPtr window, IntPtr dc);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr WindowFromPoint(POINT p);
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool ScreenToClient(IntPtr hWnd, ref POINT lpPoint);
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern bool PostMessage(IntPtr hWnd, uint Msg, uint wParam, IntPtr lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        public Window Window { get; private set; }

        public POINT lastPoint { get; private set; }

        public WindowService(IntPtr hWnd)
        {
            this.Window = new Window(hWnd);
        }

        /// <summary>
        /// Creates Window instance for the object in the specified point of screen
        /// </summary>
        /// <param name="globalPoint"></param>
        public WindowService(POINT globalPoint)
        {
            POINT screenPoint = new POINT(globalPoint);
            IntPtr hWnd = WindowFromPoint(screenPoint);
            this.Window = new Window(hWnd);
        }

        //public String GetTitle()
        //{
        //    if (this.Window.Title == null)
        //    {
        //        StringBuilder title = new StringBuilder();
        //        GetWindowText(this.Window.HWnd, title, 0);
        //        this.Window.Title = title.ToString();
        //    }
        //    return this.Window.Title;
        //}

        /// <summary>
        /// Returns point relative to current window
        /// </summary>
        /// <param name="globalPoint"></param>
        /// <returns></returns>
        public POINT GetRelativePoint(POINT globalPoint)
        {
            POINT relPoint = new POINT(globalPoint);
            ScreenToClient(this.Window.HWnd, ref relPoint);
            return relPoint;
        }

        public Color GetColorAt()
        {
            return this.GetColorAt(this.lastPoint);
        }

        public Color GetColorAt(POINT relativePoint)
        {
            Color color;

            this.lastPoint = relativePoint;

            IntPtr windowDC = GetDC(this.Window.HWnd);

            int pixel = (int)GetPixel(windowDC, relativePoint.X, relativePoint.Y);

            ReleaseDC(this.Window.HWnd, windowDC);

            color = Color.FromArgb(255,
                (byte)((pixel >> 0) & 0xff),
                (byte)((pixel >> 8) & 0xff),
                (byte)((pixel >> 16) & 0xff));

            return color;
        }

        public void SendKey(uint key)
        {
            PostMessage(this.Window.HWnd, KeyUtils.WM_KEYDOWN, key, IntPtr.Zero);
            //PostMessage(this.HWnd, KeyUtils.WM_CHAR, key, IntPtr.Zero);
            PostMessage(this.Window.HWnd, KeyUtils.WM_KEYUP, key, IntPtr.Zero);
        }

        public void MouseClick()
        {
            PostMessage(this.Window.HWnd, MouseUtils.WM_LBUTTONDOWN, MouseUtils.MK_LBUTTON, (IntPtr)((this.lastPoint.Y << 16) | (this.lastPoint.X & 0xFFFF)));
            PostMessage(this.Window.HWnd, MouseUtils.WM_LBUTTONUP, MouseUtils.MK_LBUTTON, (IntPtr)((this.lastPoint.Y << 16) | (this.lastPoint.X & 0xFFFF)));
        }
    }
}
