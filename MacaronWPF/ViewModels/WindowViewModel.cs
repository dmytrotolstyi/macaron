﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MacaronWPF.ViewModels
{
    public class WindowViewModel : BindableBase
    {
        #region [Class Fields Definition]
        private Window _Window;
        public Window Window
        {
            get
            {
                return this._Window;
            }
            private set
            {
                SetProperty(ref this._Window, value);
            }
        }
        #endregion

        #region [Commands Definition]
        public ICommand StartActionCommand { get; }
        public ICommand PauseActionCommand { get; }
        public ICommand StopActionCommand { get; }
        #endregion

        public WindowViewModel(Window window)
        {
            this._Window = window;

            this.StartActionCommand = new DelegateCommand<String>(this.StartAction);
        }

        private void StartAction(String actionName)
        {
            Console.WriteLine($"Starting action with name: [{actionName}]");
        }

        private void PauseAction(String actionName)
        {
            Console.WriteLine($"Pausing action with name: [{actionName}]");
        }

        private void StopAction(String actionName)
        {
            Console.WriteLine($"Stopping action with name: [{actionName}]");
        }
    }
}
