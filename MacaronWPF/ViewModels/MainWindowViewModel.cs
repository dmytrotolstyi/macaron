﻿using MacaronWPF.Structures;
using MacaronWPF.Utils;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace MacaronWPF.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        public ICommand GetCoordinatesCommand { get; }

        public MainWindowViewModel()
        {
            this.GetCoordinatesCommand = new DelegateCommand(this.GetCoordinates);
        }

        private void GetCoordinates()
        {
            POINT pt = MouseUtils.GetCursorPosition();
            Console.WriteLine($"Cursor position is: ({pt.X};{pt.Y})");
            //GetCursorPos(out pt);
            //this.point = new Point(pt.X, pt.Y);
            //POINT relPoint = GetRelativePoint(point);

            //this.window = new Window(pt);
            //this.pbColorPicked.BackColor = window.GetColorAt();
            //this.tbHWnd.Text = this.window.HWnd.ToString();
        }
    }
}
