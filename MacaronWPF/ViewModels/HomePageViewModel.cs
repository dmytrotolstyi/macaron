﻿using MacaronWPF.Services;
using MacaronWPF.Structures;
using MacaronWPF.Utils;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Input;

namespace MacaronWPF.ViewModels
{
    public class HomePageViewModel : BindableBase
    {

        #region [Class Fields Definition]
        public String ProcessName { get; set; }

        private Process[] _Processes;
        public Process[] Processes
        {
            get
            {
                return this._Processes;
            }
            private set
            {
                SetProperty(ref this._Processes, value);
            }
        }

        private Process _SelectedProcess;
        public Process SelectedProcess
        {
            get
            {
                return this._SelectedProcess;
            }
            set
            {
                SetProperty(ref this._SelectedProcess, value);
                this._Windows = ProcessService.GetWindowsByProcess(value).Select(window => new WindowViewModel(window));
            }
        }

        private IEnumerable<WindowViewModel> _Windows;
        public IEnumerable<WindowViewModel> Windows
        {
            get
            {
                return this._Windows;
            }

            set
            {
                SetProperty(ref this._Windows, value);
            }
        }

        private Window _SelectedWindow;
        public Window SelectedWindow
        {
            get
            {
                return this._SelectedWindow;
            }

            set
            {
                SetProperty(ref this._SelectedWindow, value);
            }
        }

        // Launch properties
        private String _ApplicationPath;
        public String ApplicationPath
        {
            get
            {
                return this._ApplicationPath;
            }

            set
            {
                SetProperty(ref this._ApplicationPath, value);
            }
        }

        private int _LaunchCount;
        public int LaunchCount
        {
            get
            {
                return this._LaunchCount;
            }

            set
            {
                SetProperty(ref this._LaunchCount, value);
            }
        }

        private String[][] Credentials { get; set; }
        #endregion

        #region [Commands Definition]
        public object WindowTitle { get; set; }

        public ICommand GetProcessesCommand { get; }
        public ICommand GetWindowsCommand { get; }
        public ICommand SendKeysCommand { get; }
        public ICommand StartCommand { get; }
        public ICommand TestCommand { get; }

        // Launch commands
        public ICommand LaunchInstancesCommand { get; }

        // Selection commands
        public ICommand StartActionSelectedCommand { get; }
        #endregion

        public HomePageViewModel()
        {
            this.ProcessName = "l2";
            this.WindowTitle = "Lineage II";

            this.ApplicationPath = @"D:\Games\Lineage_II_Int_la2world\system\l2.exe";
            this.LaunchCount = 6;

            this.Credentials = new String[][] {
            new String[] {"wu_whiteangel", "drgalaxy"},
            new String[] {"hv_whiteangel", "drgalaxy"},
            new String[] {"cf_whiteangel", "drgalaxy"},
            new String[] {"da_whiteangel", "drgalaxy"},
            new String[] {"sy_whiteangel", "drgalaxy"},
            new String[] {"vh_whiteangel", "drgalaxy"}
            };

            this.GetProcessesCommand = new DelegateCommand(this.GetProcesses);
            this.GetWindowsCommand = new DelegateCommand(this.GetWindows);
            this.SendKeysCommand = new DelegateCommand(this.SendKeys);
            this.StartCommand = new DelegateCommand(this.Start);
            this.TestCommand = new DelegateCommand(this.Test);

            // Launch commands
            this.LaunchInstancesCommand = new DelegateCommand(this.LaunchInstances);

            // Selection commands
            this.StartActionSelectedCommand = new DelegateCommand(this.StartActionSelected);
        }

        private void StartActionSelected()
        {
            if (this.Windows.ToList().Count > 0)
            {
                var i = 0;
                foreach (WindowViewModel windowVM in this.Windows)
                {
                    AlgorithmUtils.Start("Login", windowVM.Window, this.Credentials[i]);
                    i++;
                }
            }
        }

        private void GetProcesses()
        {
            this.Processes = ProcessService.GetProcessByName(this.ProcessName);
        }

        private void GetWindows()
        {
            if (this.ProcessName != null && this.WindowTitle != null)
            {
                this.GetProcesses();

                var windows = new List<Window>();
                this.Windows = this.Processes
                    .SelectMany(process => ProcessService.GetWindowsByProcess(process))
                    .Where(window => window.Title.Equals(this.WindowTitle))
                    .Select(window => new WindowViewModel(window));
            }
        }

        private void SendKeys()
        {
            foreach (Window window in this.Windows.Select(windowVM => windowVM.Window))
            {
                window.SendString("Hello");
            }
        }

        private void Start()
        {
            foreach(Window window in this.Windows.Select(windowVM => windowVM.Window))
            {
                AlgorithmUtils.Start("Farm", window);
            }
            //Algorithm.RunAll(this.Windows.Select(windowVM => windowVM.Window).ToList());
            Console.WriteLine("After the call to the method async");
        }

        private void Test()
        {
            //Console.WriteLine(AlgorithmUtils.GetNames().ToArray());
            AlgorithmUtils.Start("Test", this.Windows.First().Window);
            //Algorithm.Test();
        }

        private void LaunchInstances()
        {
            if (!String.IsNullOrEmpty(this.ApplicationPath) && File.Exists(this.ApplicationPath))
            {
                if (this.LaunchCount > 0)
                {
                    for (var i = 0; i < this.LaunchCount; i++)
                    {
                        Process.Start(this.ApplicationPath);
                    }
                }
            }
        }
    }
}
