﻿using MahApps.Metro;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MacaronWPF.ViewModels
{
    public class TemplateManagerViewModel : BindableBase
    {
        private String _Color;
        public String Color
        {
            get
            {
                return this._Color;
            }
            set
            {
                Console.WriteLine($"Selected color is: {value}");
                ThemeManager.ChangeAppStyle(Application.Current,
                                    ThemeManager.GetAccent(value),
                                    ThemeManager.GetAppTheme("BaseDark"));
                this._Color = value;
            }
        }
        public ObservableCollection<String> Colors { get; private set; }

        public TemplateManagerViewModel()
        {
            this.Colors = new ObservableCollection<string>() { "Red", "Green", "Blue", "Purple", "Orange", "Lime", "Emerald", "Teal", "Cyan", "Cobalt", "Indigo", "Violet", "Pink", "Magenta", "Crimson", "Amber", "Yellow", "Brown", "Olive", "Steel", "Mauve", "Taupe", "Sienna" };
            this.Color = "Red";
        }
    }
}
