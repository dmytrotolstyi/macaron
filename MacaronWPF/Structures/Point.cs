﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MacaronWPF.Structures
{
    [StructLayout(LayoutKind.Sequential)]
    public struct POINT
    {
        public int X;
        public int Y;

        public POINT(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public POINT(System.Windows.Point pt) : this((int)pt.X, (int)pt.Y) { }

        public static implicit operator System.Windows.Point(POINT p)
        {
            return new System.Windows.Point(p.X, p.Y);
        }

        public static implicit operator POINT(System.Windows.Point p)
        {
            return new POINT((int)p.X, (int)p.Y);
        }
    }
}
