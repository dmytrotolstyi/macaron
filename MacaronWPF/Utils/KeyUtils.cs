﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MacaronWPF.Utils
{
    class KeyUtils
    {
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool PostMessage(IntPtr hWnd, uint Msg, uint wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);

        public const uint WM_KEYDOWN = 0x0100;
        public const uint WM_KEYUP = 0x0101;
        public const uint WM_CHAR = 0x0102;

        public const uint VK_F1 = 0x0070;
        public const uint VK_F2 = 0x0071;
        public const uint VK_F3 = 0x0072;
        public const uint VK_F4 = 0x0073;
        public const uint VK_F5 = 0x0074;
        public const uint VK_F6 = 0x0075;
        public const uint VK_F7 = 0x0076;
        public const uint VK_F8 = 0x0077;
        public const uint VK_F9 = 0x0078;
        public const uint VK_F10 = 0x0079;
        public const uint VK_F11 = 0x007A;
        public const uint VK_F12 = 0x007B;

        public const uint VK_TAB = 0x09;
        public const uint VK_ENTER = 0x0D;

        public static void SendKey(IntPtr hWnd, uint key)
        {
            PostMessage(hWnd, KeyUtils.WM_KEYDOWN, key, IntPtr.Zero);
            PostMessage(hWnd, KeyUtils.WM_KEYUP, key, IntPtr.Zero);
        }

        public static void SendKey2(IntPtr hWnd, uint key)
        {
            PostMessage(hWnd, KeyUtils.WM_KEYDOWN, key, IntPtr.Zero);
            PostMessage(hWnd, KeyUtils.WM_CHAR, key, new IntPtr(0x000f0001));
            PostMessage(hWnd, KeyUtils.WM_KEYUP, key, IntPtr.Zero);
        }

        public static void SendChar(IntPtr hWnd, uint character)
        {
            PostMessage(hWnd, KeyUtils.WM_CHAR, character, IntPtr.Zero);
        }
    }
}
