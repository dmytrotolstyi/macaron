﻿using MacaronWPF.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MacaronWPF.Utils
{
    class MouseUtils
    {
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetCursorPos(out POINT lpPoint);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetCursorPos(int x, int y);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool PostMessage(IntPtr hWnd, uint Msg, uint wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        public const uint WM_LBUTTONDOWN = 0x0201;
        public const uint WM_LBUTTONUP = 0x0202;

        private const int MOUSEEVENTF_MOVE = 0x0001;
        private const int MOUSEEVENTF_LEFTDOWN = 0x0002;
        private const int MOUSEEVENTF_LEFTUP = 0x0004;
        private const int MOUSEEVENTF_ABSOLUTE = 0x8000;

        public const uint MK_LBUTTON = 0x001;

        public static POINT GetCursorPosition()
        {
            POINT pt = new POINT();
            GetCursorPos(out pt);
            return pt;
        }

        public static void Click(IntPtr hWnd, POINT relativePoint)
        {
            PostMessage(hWnd, MouseUtils.WM_LBUTTONDOWN, MouseUtils.MK_LBUTTON, (IntPtr)((relativePoint.Y << 16) | (relativePoint.X & 0xFFFF)));
            PostMessage(hWnd, MouseUtils.WM_LBUTTONUP, MouseUtils.MK_LBUTTON, (IntPtr)((relativePoint.Y << 16) | (relativePoint.X & 0xFFFF)));
        }

        public static void MoveTo(POINT p)
        {
            //mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE, p.X, p.Y, 0, 0);
            SetCursorPos(p.X, p.Y);
        }

        public static void LeftClick(POINT p)
        {
            mouse_event(MOUSEEVENTF_LEFTDOWN, p.X, p.Y, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, p.X, p.Y, 0, 0);
        }
    }
}
