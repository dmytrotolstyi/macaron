﻿using MacaronWPF.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MacaronWPF.Utils
{
    class AlgorithmUtils
    {
        /// <summary>
        /// Returns algorithm object according to it's class name
        /// Note: Search is done inside Algorithm.cs class namespace, that's why all algorithms should be stored under this namespace
        /// </summary>
        /// <param name="name">Algorithm class name (e.g. "Login")</param>
        /// <returns>Algorithm object in case of sucess or null otherwise</returns>
        public static Algorithm Get(String name)
        {
            Algorithm algorithm = null;

            Type type =  Type.GetType(typeof(Algorithm).Namespace + "." + name);

            if (type != null)
            {
                ConstructorInfo ctor = type.GetConstructor(new Type[] { });
                algorithm = (Algorithm)ctor.Invoke(new object[] { });
            }

            return algorithm;
        }

        /// <summary>
        /// Starts algorithm according to it's class name
        /// </summary>
        /// <param name="name">Algorithm class name (e.g. "Login")</param>
        /// <returns>"true" if start was successful or "false" otherwise</returns>
        public static bool Start(String name, Window window, String[] parameters = null)
        {
            Algorithm algorithm = AlgorithmUtils.Get(name);

            if (algorithm != null)
            {
                algorithm.Start(window, parameters);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Retrieves list of names of all existing algorithms
        /// </summary>
        /// <returns></returns>
        public static List<String> GetNames()
        {
            return Assembly.GetExecutingAssembly().GetTypes()
                      .Where(type => type.IsSubclassOf(typeof(Algorithm)) && !type.IsAbstract)
                      .Select(type => type.Name)
                      .Where(name => !name.Equals("Algorithm")) // Excluding abstract base class
                      .ToList();
        }
    }
}
