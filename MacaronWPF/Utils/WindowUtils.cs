﻿using MacaronWPF.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MacaronWPF.Utils
{
    class WindowUtils
    {

        private const int X_CORNER_PADDING = 100;
        private const int Y_CORNER_PADDING = 50;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);

        public static void MoveToTheCorner(IntPtr hWnd)
        {
            // TODO: Check case with multiple monitors
            Console.WriteLine($"Size is: {System.Windows.SystemParameters.PrimaryScreenWidth}:{System.Windows.SystemParameters.PrimaryScreenHeight}");
        }
    }
}
