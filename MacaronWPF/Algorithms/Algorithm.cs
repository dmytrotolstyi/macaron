﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacaronWPF.Algorithms
{
    abstract class Algorithm
    {
        String Title { get; }

        String Description { get; }

        public Algorithm(String title, String description)
        {
            this.Title = title;
            this.Description = description;
        }

        public async void Start(Window window, String[] parameters = null)
        {
            Task task = Task.Run(() => Execute(window, parameters));
            await task;
        }

        public abstract void Execute(Window window, String[] paramters = null);
    }
}
