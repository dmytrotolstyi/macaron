﻿using MacaronWPF.Structures;
using MacaronWPF.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MacaronWPF.Algorithms
{
    class Clicker : Algorithm
    {
        const String TITLE = "Clicker";
        const String DESCRIPTION = "Activates specified window and infinetely clicks at the same coordinates";

        public Clicker() : base(TITLE, DESCRIPTION)
        {

        }

        public override void Execute(Window window, String[] parameters = null)
        {
            window.BringToFront();

            Thread.Sleep(2000);

            for (int i = 0; i < 10000; i++)
            {
                MouseUtils.LeftClick(new POINT());
                Thread.Sleep(200);
            }
        }
    }
}
