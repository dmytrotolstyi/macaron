﻿using MacaronWPF.Structures;
using MacaronWPF.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MacaronWPF.Algorithms
{
    class Farm : Algorithm
    {
        const String TITLE = "Farm";
        const String DESCRIPTION = "Farming mobs using panel to cast skills and search next targets";

        private static readonly POINT lastHPPoint = new POINT(16, 28);
        private static readonly Color hpColor = Color.FromArgb(0xFF, 0xD6, 0x18, 0x42);

        public Farm() : base(TITLE, DESCRIPTION)
        {
        }

        public override void Execute(Window window, String[] parameters = null)
        {
            window.SendKey(KeyUtils.VK_F5);
            Random rnd = new Random();
            Color currentColor;

            while (true)
            {
                for (int j = 0; j < 5; j++)
                {
                    for (int i = 0; i < 50; i++)
                    {
                        // Waiting for mob to appear
                        Console.WriteLine("Searching mob");
                        while ((currentColor = window.GetColorAt(lastHPPoint)) != hpColor)
                        {
                            Console.WriteLine("Mob not found");
                            window.SendKey(KeyUtils.VK_F1);
                            Thread.Sleep(200 + rnd.Next(0, 200));
                        }

                        // Killing mob
                        while ((currentColor = window.GetColorAt(lastHPPoint)) == hpColor)
                        {
                            Console.WriteLine("Killing mob");
                            window.SendKey(KeyUtils.VK_F2);
                            Thread.Sleep(350 + rnd.Next(0, 100));
                        }
                    }

                    // Putting money into bank
                    window.SendKey(KeyUtils.VK_F5);
                    Thread.Sleep(150 + rnd.Next(0, 100));
                }

                // Rebuff
                window.SendKey(KeyUtils.VK_F6);
                Thread.Sleep(6000);
            }
        }
    }
}
