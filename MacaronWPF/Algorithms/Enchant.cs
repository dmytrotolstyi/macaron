﻿using MacaronWPF.Structures;
using MacaronWPF.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MacaronWPF.Algorithms
{
    class Enchant : Algorithm
    {
        const String TITLE = "Enchant";
        const String DESCRIPTION = "Performs enchanting of the items in fixed slots until the max level of enchant";

        const int MAX_ENCHANT = 17;

        public Enchant() : base(TITLE, DESCRIPTION)
        {
        }

        public override void Execute(Window window, String[] parameters = null)
        { 
            POINT enchWindowCheckPoint = new POINT(27, 481);
            Color enchWindowCheckColor = Color.FromArgb(0xFF, 0x10, 0x10, 0x10);

            POINT enchSuccessCheckPoint = new POINT(30, 624);
            Color enchSuccessCheckColor = Color.FromArgb(0xFF, 0xFF, 0xFB, 0x00);

            POINT enchApplyButtonPoint = window.GetAbsolutePoint(new POINT(88, 512));

            POINT[] slotsPoints = new POINT[]
            {
                window.GetAbsolutePoint(new POINT(23, 195)),
                window.GetAbsolutePoint(new POINT(63, 195)),
                window.GetAbsolutePoint(new POINT(99, 195)),
                window.GetAbsolutePoint(new POINT(136, 195)),
                window.GetAbsolutePoint(new POINT(173, 195)),
                window.GetAbsolutePoint(new POINT(210, 195))
            };

            int[] slotsEnchLevels = new int[] { 6, 5, 3, 9, 4, 5 };

            window.BringToFront();

            int cou = 0;

            while (!Array.TrueForAll(slotsEnchLevels, (value) => { return value >= MAX_ENCHANT; }) && cou <= 14000)
            {
                for (int i = 0; i < slotsPoints.Length; i++)
                {
                    if (slotsEnchLevels[i] >= MAX_ENCHANT)
                    {
                        continue;
                    }
                    cou++;
                    window.SendKey(KeyUtils.VK_F5);
                    Thread.Sleep(300);
                    while (window.GetColorAt(enchWindowCheckPoint) != enchWindowCheckColor)
                    {
                        window.SendKey(KeyUtils.VK_F5);
                        Thread.Sleep(600);
                    }

                    MouseUtils.MoveTo(slotsPoints[i]);
                    Thread.Sleep(300);

                    MouseUtils.LeftClick(slotsPoints[i]);
                    Thread.Sleep(200);

                    MouseUtils.MoveTo(enchApplyButtonPoint);
                    Thread.Sleep(300);

                    MouseUtils.LeftClick(enchApplyButtonPoint);
                    Thread.Sleep(500);

                    Color clr = window.GetColorAt(enchSuccessCheckPoint);
                    Console.WriteLine($"Color is: [{clr}]");
                    if (clr == enchSuccessCheckColor)
                    {
                        slotsEnchLevels[i]++;
                        Console.WriteLine($"Ench for slot [{i}] is successful! Current ench is: [{slotsEnchLevels[i]}]");
                    }
                    else
                    {
                        slotsEnchLevels[i] = 3;
                        Console.WriteLine($"Ench failed for slot [{i}]. Current ench is: [{slotsEnchLevels[i]}]");
                    }
                    Thread.Sleep(1000);
                }
                Console.WriteLine($"Current ench levels: [{String.Join(", ", slotsEnchLevels)}]");
            }
            Console.WriteLine("Ench process is successfully finished");
        }
    }
}
