﻿using MacaronWPF.Structures;
using MacaronWPF.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MacaronWPF.Algorithms
{
    class Login : Algorithm
    {
        const String TITLE = "Login";
        const String DESCRIPTION = "Enters login credentials";

        public Login() : base(TITLE, DESCRIPTION)
        {
        }

        public override void Execute(Window window, String[] parameters = null)
        {
            window.SendString(parameters[0]);
            //Thread.Sleep(300);
            //window.SendKey(KeyUtils.VK_TAB);
            //Thread.Sleep(300);
            //window.SendString(parameters[1]);
            //Thread.Sleep(300);
            //window.SendKey(KeyUtils.VK_ENTER);
        }
    }
}
