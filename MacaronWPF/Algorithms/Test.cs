﻿using MacaronWPF.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacaronWPF.Algorithms
{
    class Test : Algorithm
    {
        public Test() : base("", "")
        {
        }

        public override void Execute(Window window, string[] paramters = null)
        {
            KeyUtils.SendKey2(window.HWnd, 'A');
            KeyUtils.SendKey2(window.HWnd, KeyUtils.VK_TAB);
            KeyUtils.SendKey2(window.HWnd, 'A');
        }
    }
}
