﻿using MacaronWPF.Structures;
using MacaronWPF.Utils;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace MacaronWPF
{


    public class Window
    {
        #region [Win32API Imports]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetDC(IntPtr window);
        [DllImport("gdi32.dll", SetLastError = true)]
        public static extern uint GetPixel(IntPtr dc, int x, int y);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int ReleaseDC(IntPtr window, IntPtr dc);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr WindowFromPoint(POINT p);
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool ScreenToClient(IntPtr hWnd, ref POINT lpPoint);
        [DllImport("user32.dll")]
        static extern bool ClientToScreen(IntPtr hWnd, ref POINT lpPoint);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetWindowTextLength(IntPtr hWnd);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        #endregion

        public IntPtr HWnd { get; private set; }

        private String _Title;
        public String Title
        {
            get
            {
                GetTitle();
                return this._Title;
            }
            private set
            {
                this._Title = value;
            }
        }
        public POINT lastPoint { get; private set; }

        public Window(IntPtr hWnd)
        {
            this.HWnd = hWnd;

            this.InitCommands();
        }

        public ICommand StartActionCommand { get; set; }

        /// <summary>
        /// Creates Window instance for the object in the specified point of screen
        /// </summary>
        /// <param name="globalPoint"></param>
        public Window(POINT globalPoint)
        {
            POINT screenPoint = new POINT(globalPoint);
            this.HWnd = WindowFromPoint(screenPoint);

            this.InitCommands();
        }


        private void InitCommands()
        {
            this.StartActionCommand = new DelegateCommand<String>(this.StartAction);
        }

        private void GetTitle()
        {
            if (this._Title == null)
            {
                int length = GetWindowTextLength(this.HWnd);
                if (length > 0)
                {
                    StringBuilder title = new StringBuilder(length + 1);
                    GetWindowText(this.HWnd, title, title.Capacity);
                    this._Title = title.ToString();
                }
            }
        }

        /// <summary>
        /// Returns point relative to current window
        /// </summary>
        /// <param name="globalPoint"></param>
        /// <returns></returns>
        public POINT GetRelativePoint(POINT globalPoint)
        {
            POINT relPoint = new POINT(globalPoint);
            ScreenToClient(this.HWnd, ref relPoint);
            return relPoint;
        }

        /// <summary>
        /// Returns absolute point on the screen from the relative point of the window
        /// </summary>
        /// <param name="relativePoint"></param>
        /// <returns></returns>
        public POINT GetAbsolutePoint(POINT relativePoint)
        {
            POINT absPoint = new POINT(relativePoint);
            ClientToScreen(this.HWnd, ref absPoint);
            return absPoint;
        }

        public Color GetColorAt()
        {
            return this.GetColorAt(this.lastPoint);
        }

        public Color GetColorAt(POINT relativePoint)
        {
            Color color;

            this.lastPoint = relativePoint;

            IntPtr windowDC = GetDC(this.HWnd);

            int pixel = (int)GetPixel(windowDC, relativePoint.X, relativePoint.Y);

            ReleaseDC(this.HWnd, windowDC);

            color = Color.FromArgb(255,
                (byte)((pixel >> 0) & 0xff),
                (byte)((pixel >> 8) & 0xff),
                (byte)((pixel >> 16) & 0xff));

            return color;
        }

        public void SendKey(uint key)
        {
            KeyUtils.SendKey(this.HWnd, key);
        }

        public void SendString(String value)
        {
            foreach (char c in value)
            {
                KeyUtils.SendChar(this.HWnd, c);
            }
        }

        public void MouseClick()
        {
            MouseUtils.Click(this.HWnd, this.lastPoint);
        }

        public void MouseClick(POINT point)
        {
            MouseUtils.Click(this.HWnd, point);
        }

        public void BringToFront()
        {
            WindowUtils.SetForegroundWindow(this.HWnd);
        }

        private void StartAction(String actionName)
        {
            Console.WriteLine("Starting action: " + actionName);
        }
    }    
}
